var mymap = L.map('mapid').setView([-27.451166, -58.986508], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
}).addTo(mymap);
 
$.ajax({
    datatype : "json",
    url : "api/bicicletas",
    success : function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {
                title: 'Bicicleta ID: '+bici.id +' - '+bici.modelo+' '+bici.color}).addTo(mymap);            
        });
    }
})