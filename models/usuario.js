var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const { getMaxListeners } = require('./reserva');

const saltRounds = 10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

var usuarioSchema = new Schema({
    nombre: {
        type : String, 
        trim: true,
        required: [true, 'El nombre es obligatorio.']
    },
    email: {
        type : String, 
        trim: true,
        required: [true, 'El e-mail es obligatorio.'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un e-mail valido.'],
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password: {
        type : String, 
        required: [true, 'El password es obligatorio.']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado : {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario.'});

usuarioSchema.pre('save', function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    };
    next();
});

usuarioSchema.methods.validPassword = function(){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (idBici, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: idBici, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function (cb){
    const token = new Token({_userID: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if (err) {return console.log('EH AMEO'+err.message);}

        const mailOptions = {
            from: 'ironchauli@gmail.com',
            to: email_destination,
            subject: 'Verificacion de Cuenta',
            text: 'Hola \n\n'+ 'Por favor para verificar la cuenta haga click en el siguiente link: \n' 
            +process.env.HOST 
            + '\/token/confirmation\/'+token.token+'\n'
        };
        mailer.sendMail(mailOptions, function(err) {
            if (err) { return console.log(err.message);}
            console.log('Un email de verificacion fue enviado a:'+email_destination);
        });
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result)=> {
            if (result){
                callback(err, result)
            } else {
                console.log('---------------CONDITION----------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.picture;
                console.log('----------------VALUES----------');
                console.log(values);
                self.create(values, (err, result)=>{
                    if (err) {console.log(err);}
                    return callback(err, result)
                })
            }
    })
}

module.exports = mongoose.model('Usuario', usuarioSchema);

