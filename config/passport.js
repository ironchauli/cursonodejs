const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const Usuario = require('../models/usuario');

passport.use(new localStrategy(
    function(username, password, done) {
        Usuario.findOne({ email: email }, function(err, usuario) {
          if (err) { return done(err); }
          if (!usuario) {
            return done(null, false, { message: 'Usuario Incorrecto.' });
          }
          if (!usuario.validPassword(password)) {
            return done(null, false, { message: 'Password Incorrecto.' });
          }
          return done(null, usuario);
        });
    }
));

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.HOST + "/auth/google/callback"
},
function(accessToken, refreshToken, profile, cb) {
  console.log(profile);
  
  Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
    return cb(err, user);
  });
}
));

passport.serializeUser(function(user, cb){
    console.log('serialize----------');
    console.log(user);
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    console.log('deserialize----------');
    console.log(id);
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
});

module.exports = passport;