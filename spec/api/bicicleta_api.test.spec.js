var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';



describe('Bicicleta API', ()=>{
    
    beforeAll(function(done){
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.createConnection(mongoDb, {useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'Error conectado con Base de datos testDB'));
        db.once('open', function(){
            console.log('Conexion exitosa a testDB');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
        
    });  
    afterAll(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });  

    describe('GET BICICLETAS /', ()=>{
        it('Status 200', (done)=>{
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', ()=> {
        it('STATUS 200', (done)=> {
            var header = {'content-type': 'application/json'};
            var bici = '{ "id" : 10, "color" : "Rojo", "modelo" : "Urbana", "lat" : -27.469234, "lng" : -58.965871}';
            request.post({
                headers : header,
                url : base_url + '/create',
                body : bici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var unaBici = JSON.parse(body).bicicleta;
                console.log(unaBici);
                expect(unaBici.color).toBe("Rojo");
                expect(unaBici.modelo).toBe("Urbana");
                expect(unaBici.ubicacion[0]).toBe(-27.469234);
                expect(unaBici.ubicacion[1]).toBe(-58.965871);
                done();
            });
        });
    });
/*
    describe('POST BICICLETAS /update', ()=> {
        it('STATUS 200', (done)=> {
            
            var bici = new Bicicleta(1, 'Roja', 'Playera', [-27.469234, -58.965871]);
            Bicicleta.add(bici);

            var header = {'content-type': 'application/json'};
            var bici = '{ "id" : 1, "color" : "Verde", "modelo" : "Mountain Bike", "lat" : -27.111, "lng" : -58.222}';
            request.post({
                headers : header,
                url : base_url + '/update',
                body : bici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.getbyId(1).color).toBe("Verde");
                expect(Bicicleta.getbyId(1).modelo).toBe("Mountain Bike");
                expect(Bicicleta.getbyId(1).ubicacion[0]).toBe(-27.111);
                expect(Bicicleta.getbyId(1).ubicacion[1]).toBe(-58.222);
                done();
            });
        });
    });
*/
/*
    describe('POST BICICLETAS /delete', ()=> {
        it('STATUS 204', (done)=> {
            
            var bici = new Bicicleta.createInstance(1, 'Roja', 'Playera', [-27.469234, -58.965871]);
            Bicicleta.add(bici, function(err, newBici){
                expect(Bicicleta.allBicis.length).toBe(1);
                var header = {'content-type': 'application/json'};
                var bici = '{ "id" : 1}';
                request.post({
                    headers : header,
                    url : base_url + '/delete',
                    body : bici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(204);
                    expect(Bicicleta.allBicis.length).toBe(0);
                    done();
                });
            });         
        });
    });
    */
});

