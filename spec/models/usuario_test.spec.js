var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
const usuario = require('../../models/usuario');

describe('Testing Usuarios', function(){
    beforeAll(function(done){
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'Error conectado con Base de datos testDB'));
        db.once('open', function(){
            console.log('Conexion exitosa a testDB');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    afterAll(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    mongoose.disconnect(err); 
                    done();
                });
            });
        });
    });

    describe('Un Usuario reserva una Bici', () => {
        it('Debe existir la reserva', (done)=>{
            const usuario = new Usuario({nombre: 'Gonza'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: 'Negra', modelo: 'Mountain Bike'});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    //expect(reservas[0].bicicleta.usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});