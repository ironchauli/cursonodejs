**CursoNodeJS**

Este proyecto de prueba es un intento de incorporar nuevos conocimientos sobre programacion en Javascript, el objetivo es aprender a hacer desarrollo de APIs con Javascript para poder en algun futuro implementar backends en esta tecnologia.

---
## Instalar a la aplicacion ##

Clonar el repositorio
cd red_bicicletas
npm install
npm run devstart

## Ingreso/Uso a la aplicacion ##

Para poder ingresar a visualizar la pagina por defecto de Express 
localhost:3000/express.

Para ingresar a la aplicacion web 
localhost:3000/app

Para acceder al CRUD de bicicletas
localhost:3000/bicicletas

EndPoints de la API (con JSON de ejemplo)
READ (GET)
http://localhost:3000/bicicletas/api

CREATE (POST)
http://localhost:3000/bicicletas/api/create
{
	"id" : 3,
	"color" : "Verde",
	"modelo" : "Clasica",
	"lat" : -27.451267,
	"lng" : -58.984826
}

UPDATE (POST)
http://localhost:3000/bicicletas/api/update
{
	"id" : 2,
	"color" : "Verde Claro",
	"modelo" : "Mountain Bike Modificada",
	"lat" : -27.450226, 
	"lng" : -58.980193
}

DELETE (POST)
http://localhost:3000/bicicletas/api/delete
{
	"id" : 5
}