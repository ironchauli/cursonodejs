var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion : {
        type : [Number], index : { type : '2dsphere', sparse : true}
    }
 
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code : code,
        color : color,
        modelo : modelo,
        ubicacion : ubicacion
    });
};

bicicletaSchema.methods.toString = function() {
    return 'code: '+ this.id + " | color: "+ this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function (unaBici, cb) {
    this.create(unaBici, cb);
}

bicicletaSchema.statics.findByCode = function (unCode, cb) {
    return this.findOne({code : unCode}, cb);
}

bicicletaSchema.statics.removeByCode = function (unCode, cb) {
    return this.deleteOne({code : unCode}, cb);
}

module.exports = mongoose.model('Bicicleta' , bicicletaSchema);