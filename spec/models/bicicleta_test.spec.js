var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'Error conectado con Base de datos testDB'));
        db.once('open', function(){
            console.log('Conexion exitosa a testDB');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Creamos una instancia de Bicicleta', () =>{
            
            var bici = Bicicleta.createInstance(1, 'Negra', 'Mountain Bike', [-27.458796, -58.983151]);
            
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Negra');
            expect(bici.modelo).toBe('Mountain Bike');
            expect(bici.ubicacion[0]).toBe(-27.458796);
            expect(bici.ubicacion[1]).toBe(-58.983151);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza Vacía', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) =>{
            var unaBici = new Bicicleta(
                {code: 1,
                 color: 'Negra',
                 modelo: 'Mountain Bike'});
            Bicicleta.add(unaBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(unaBici.code);
                    expect(bicis[0].color).toEqual(unaBici.color);
                    expect(bicis[0].modelo).toEqual(unaBici.modelo);

                    done();
                });
            }); 
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Devuelve la Bici con Code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
            
                var unaBici1 = new Bicicleta(
                    {code: 1,
                     color: 'Negra',
                     modelo: 'Mountain Bike'});
                Bicicleta.add(unaBici1, function(err, newBici) {
                    if (err) console.log(err);

                    var unaBici2 = new Bicicleta(
                        {code: 2,
                         color: 'Roja',
                         modelo: 'Clasica'});
                    Bicicleta.add(unaBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toEqual(unaBici1.code);
                            expect(targetBici.color).toEqual(unaBici1.color);
                            expect(targetBici.modelo).toEqual(unaBici1.modelo);
        
                            done();
                        });
                    });
                });
            });
        });
    });
/*
    describe('Bicicleta.removeByCode', () => {
        it('Elimina la Bici con Code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
            
                var unaBici1 = new Bicicleta(
                    {code: 1,
                     color: 'Negra',
                     modelo: 'Mountain Bike'});
                Bicicleta.add(unaBici1, function(err, newBici) {
                    if (err) console.log(err);

                    var unaBici2 = new Bicicleta(
                        {code: 2,
                         color: 'Roja',
                         modelo: 'Clasica'});
                    Bicicleta.add(unaBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.removeByCode(1, function(err, targetBici){
                            if (err) console.log(err);
                            Bicicleta.allBicis(function(err, finalBicis){
                                expect(finalBicis.length).toBe(1);
                                expect(finalBicis[0].code).toEqual(unaBici2.code);
                                expect(finalBicis[0].color).toEqual(unaBici2.color);
                                expect(finalBicis[0].modelo).toEqual(unaBici2.modelo);
        
                                done();
                            });
                        });
                    });
                });
            });
        });
    });*/
});
